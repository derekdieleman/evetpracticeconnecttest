﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Microsoft.Data.Edm;
using Microsoft.Data.OData;
using Newtonsoft.Json.Linq;
using System.Text;

namespace eVetPracticeConnectTest
{
	public class APICalls
	{

		//NOTE:
		// this class creates a list that is accessible via the QueryResultList property
		// be sure to consume this property when calling this class
		const int ChunkSize = 5;
		private static string _serverURI = "https://se1.evetpractice.com/odata/";
		private string _practiceID = "407";
		private string _apiKey = "n5QY7zk2UmggOqR2uDr9sP/3I9haIqBHFA4ms4gIR5vYcDU0sx1KKCfg6HsS1soZ";
		private WebRequest _request;
		private WebResponse _response;
		private string _statusCodes = null;
		private string _QueryURL = "";
		private StreamReader _reader;
		private Stream _httpDataStream;
		private string _queryResponse;
		private string[] _parsestring;
		private Dictionary<string, JToken> _queryresultlist = new Dictionary<string, JToken>();
		private char[] delimiters = { ',' };


		public string QueryURL
		{
			get { return _QueryURL; }
			set { _QueryURL = value; }
		}

		public string QueryRepsonse
		{ 
			get { return _queryResponse; }
			set { _queryResponse = value; }
		}
		public Dictionary<string, JToken> QueryResultList
		{ 
			get { return _queryresultlist; }
			set { _queryresultlist = value; }
		}

		public string StatusCode
		{ 
			get { return _statusCodes; }
			set { _statusCodes = value; }
		}

		//This takes a build URL and transmits it to the server takes a respopnse and stores it to a string called _queryResponse
		private string SubmitQuery(string url, string apiKey, string practiceId)
		{
			WebRequest request;
			WebResponse response;
			string statusCodes = null;
			StreamReader reader;
			Stream httpDataStream;
			string queryResponse;

			request = WebRequest.Create(url);
			request.Headers.Add("APIKEY", apiKey);
			request.Headers.Add("PRACTICEID", practiceId);

			//Store the response
			response = request.GetResponse();
			//store response status codes
			statusCodes = (((HttpWebResponse)response).StatusDescription);
			//get the response data stream
			httpDataStream = response.GetResponseStream();
			//setup the stream reader to read the stream
			reader = new StreamReader(httpDataStream);
			//read the stream to the end and store the value in a string formated for JSON
			queryResponse = reader.ReadToEnd();
			//close the reader and response streams
			reader.Close();
			response.Close();
			return queryResponse;

		}
		//This builds a initial query string grabs the URI from the returned server list and builds a query using the URI.
		//It then appends a query string to the URI and builds a URL
		//Submit Query is called to actually transmit the URL requests
		public void GetQueryURL(string tablequery)
		{
			//Request The Server Specific info for the practice

			//SubmitQuery(_hostname);
			//SubmitQuery(tablequery);

			ParseServerURI(tablequery, _practiceID);

			//Split the string
			_parsestring = _queryResponse.Split(delimiters);
			//call query url
			_QueryURL = _QueryURL + @"odata/" + tablequery;
            SubmitQuery(_QueryURL, _apiKey, _practiceID);

			//Parse to Json
			//ParseResponseToJSON();
		}

		//This parses the response to Json for json objects
		//do not use this method to parse inner array values returned in queries
		//makes the response more readalbe
		public Dictionary<string, JToken> ParseResponseToJSON(string jsonToParse)
		{
			Dictionary<string, JToken> jsonDictionary = new Dictionary<string, JToken>();
			//clear the dictionary that stores the parsed response
			jsonDictionary.Clear();

			//place each split string into a dictionary that is property accessile
			var jsonresponse = JObject.Parse(jsonToParse);
			foreach (var r in jsonresponse)
			{
				jsonDictionary.Add(r.Key, r.Value);
			}

			return jsonDictionary;
		}

		//build the initial practice URI so that we talk to the right server with our queies
		private string ParseServerURI(string practiceId, string apiKey)
		{
			string SERVER_QUERY = @"Practices(" + practiceId + ")?$select=Server &$expand=Server($select=URI)";
			string startingServerURL = @"https://se1.evetpractice.com/odata/";
			string fullServerQueryURLString = startingServerURL + SERVER_QUERY;
			string practiceSpecificServerURI = null;
			string serverQueryResponse = null;
			Dictionary<string, JToken> ServerDictionary = new Dictionary<string, JToken>();
			_practiceID = practiceId;
			_apiKey = apiKey;
			//submit the server query and parse it
			serverQueryResponse = SubmitQuery(fullServerQueryURLString, _apiKey, _practiceID);
			// Iterate through the response and store it to the dictionary so that the server urI can be located
			ServerDictionary.Clear();
			ServerDictionary = ParseResponseToJSON(serverQueryResponse);

			// Grab the key value pair that relates to the server URI and store 
			practiceSpecificServerURI = ServerDictionary["Server"].ToString();
			ServerDictionary.Clear();
			ServerDictionary = ParseResponseToJSON(practiceSpecificServerURI);
			practiceSpecificServerURI = ServerDictionary["URI"].ToString();
			//_QueryURL = _queryResponse;
			return practiceSpecificServerURI;

		}

		//takes a dictionary of client IDs with the key being passed in as an arbitrary number from for loop
		//this may need to use a list instead of a dictionary.
		public void SearchByClientID(string clientsquery, string patientsquery)
		{
			GetQueryURL(clientsquery);
			List<int> Clientidresultset = new List<int>();
			List<int> TakeList = new List<int>();
			StringBuilder sbuilder;
			int count = 0;

			//get client IDs
			HashSet<int> clientIds = ParseClientIds();

			foreach (int i in clientIds)
			{
				TakeList.Add(i);
			}

			//while loop everything below this point

			//get the hashset size and if over 50 take 50 and if under 50 take remaining ids
			if (TakeList.Count > 50)
			{
				var takeresult = TakeList.Take(50);
				Clientidresultset = takeresult.ToList();
			}
			else if (TakeList.Count < 50)
			{
				int listCount = TakeList.Count;
				var takeresult = TakeList.Take(listCount);
				Clientidresultset = TakeList.ToList();
			}
			else
			{
				throw new IndexOutOfRangeException();
			}

			//build a query string using string builder to concat to the end of the date query
			//this uses the count variable to ensure the first itieration puts and in the string instead of or

			GetQueryURL(patientsquery);

			sbuilder = new StringBuilder(_QueryURL);

			foreach (int c in Clientidresultset)
			{
				if (count <= 0)
				{
					sbuilder.Append("?$filter=(Client_Id eq " + c + ")");
					count++;
				}
				else
				{ 
					sbuilder.Append(" or (Client_Id eq " + c + ")");
				}

			}
			_QueryURL = sbuilder.ToString();

			//submit and parse the client ID query
			SubmitQuery(_QueryURL, _apiKey, _practiceID);
			//ParseResponseToJSON();
		}

		//build the hash set of unque client ids
		private HashSet<int> ParseClientIds()
		{ 
			_queryResponse = _queryresultlist["value"].ToString();
			JArray clients = JArray.Parse(_queryResponse);
			HashSet<int> tempclientlist = new HashSet<int>();
			foreach (JObject content in clients.Children<JObject>())
			{
				foreach (JProperty jprop in content.Properties())
				{
					tempclientlist.Add(Convert.ToInt32(jprop.Value));
				}

			}
			return tempclientlist;
		}
		public void GetServerClientAction(string practiceId, string apiKey)
		{
			_serverURI = ParseServerURI(practiceId, apiKey);
		}



	}
}
