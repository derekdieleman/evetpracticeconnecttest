﻿using System;
namespace eVetPracticeConnectTest
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			DateTime startdate = new DateTime(2016, 05, 23, 00, 00, 00);
			Console.WriteLine("Beginning API calls for eVetPractice");
			APICalls apiCalls = new APICalls();

			//Test Action calls
			apiCalls.GetServerClientAction("407", "n5QY7zk2UmggOqR2uDr9sP/3I9haIqBHFA4ms4gIR5vYcDU0sx1KKCfg6HsS1soZ");


			//Test Queies
			apiCalls.GetQueryURL(@"Practices(407)?$select=Server &$expand=Server($select=URI)");
			//Itterate through transactions initial query


			foreach (var r in apiCalls.QueryResultList)
			{
				Console.WriteLine(r.Key.ToString() + " " + ":" + " " + r.Value.ToString());
			}
			Console.WriteLine("End Transaction Data Calls");
			Console.ReadLine();


			//Transactions Query
			apiCalls.GetQueryURL(@"Invoices?$select=Id,Total,Client_Id,IsDeleted,Discount,CreatedByUser_Id,InvoiceNumber,Practice_Id,DateCreated,InvoiceStatus &$orderby=Id &$filter=(cast(DateCreated,'Edm.DateTimeOffset') gt 2016-03-01T00:00:00-06:00) and (cast(DateCreated,'Edm.DateTimeOffset') lt 2016-07-01T00:00:00-06:00)");
			//Itterate through transactions initial query
			foreach (var r in apiCalls.QueryResultList)
			{
				Console.WriteLine(r.Key.ToString() + " " + ":" + " " + r.Value.ToString());
			}
			Console.WriteLine("End Transaction Data Calls");
			Console.ReadLine();



			//call to create a hashset list of client IDs and pull back owners associated with those clients to get the patient IDs
			apiCalls.SearchByClientID(@"PaymentHistories?$select=Client_Id &$orderby=Id &$filter=(cast(DateCreated,'Edm.DateTimeOffset') gt 2016-03-01T00:00:00-06:00) and (cast(DateCreated,'Edm.DateTimeOffset') lt 2016-07-01T00:00:00-06:00)", @"Ownerships");


			//Itterate through the Search Client ID list
			foreach (var r in apiCalls.QueryResultList)
			{
				Console.WriteLine(r.Key.ToString() + " " + ":" + " " + r.Value.ToString());
			}
			Console.WriteLine("End Client ID List");
			Console.ReadLine();
		}
	}
}
